# Input: Funktionen

> Die prozedurale Programmierung ergänzt das imperative Konzept aufeinander folgender Befehle um den Ansatz, einen Algorithmus in überschaubare Teile zu zerlegen. Je nach Programmiersprache werden diese Teile Unterprogramm, Routine, Prozedur oder Funktion genannt.
>
> *Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Prozedurale_Programmierung)*

Funktionen sind also ein Mittel, um **einen Algorithmus in überschaubare Teile zu zerlegen**. Die Begriffe *Unterprogramm*, *Routine* oder *Prozedur* sind in C# eher unüblich. Auch *Funktionen* sind in C# eher exotische Sprachkonstrukte. Typischerweise werden mit C# Applikationen nach einem objektorientierten oder klassenbasierten Ansatz entwickelt. Dabei werden "Funktionen" innerhalb einer *Klasse* geschrieben und daher als *Methoden* bezeichnet.

Für den Moment - ohne genauere Kenntnisse der objektorientierten Programmierung - ist diese Terminologie noch nicht besonders relevant. Es reicht vorerst aus, *Funktionen* und *Methoden* als "gleiche" Konstrukte zu kennen, welche abhängig vom Ort Ihrer Definition, unterschiedlich bezeichnet werden. Dabei ist es sicher auch hilfreich zu wissen, dass sämtliches Wissen über *Funktionen* später ganz direkt auch auf *Methoden* angewendet werden kann.

## Definition einer Funktion

Eine Funktion kann definiert werden, indem Anweisungen zu einem Code-Block zusammengefasst und benannt werden. Jede Funktion hat also einen **Namen**. Zusätzlich zu diesem Namen können für eine Funktion der **Typ des Rückgabewertes** sowie eine **Parameterliste** definiert werden. Der eigentliche Inhalt der Funktion, die Anweisungen, werden zwischen geschweifte Klammern geschrieben.

```
<Typ des Rückgabewertes> <Name der Funktion>(<Parameterliste>)
{
    <Anweisungen>   // Inhalt der Funktion
}
```

Die drei Bestandteile, welche vor dem Inahlt der Methode aufgeführt werden, Typ des Rückgabewertes, Funktionsname und Parameterliste, werden zusammen als **Signatur** bezeichnet.

### Typ des Rückgabewertes
Eine Funktion kann, muss jedoch nicht, einen Wert zurückgeben. Die Rückgabe eines Wertes erfolgt durch eine Anweisung, welche mit dem Schlüsselwort `return` eingeleitet wird. Sofern ein Wert zurückgegeben wird, muss in der Datentyp dieses Rückgabewertes in der Signatur aufgeführt werden. Damit wird ausgewiesen, dass jeder Aufruf dieser Funktion zur Rückgabe eines Wertes des spezifizierten Datentyps führt.
Wenn die Funktion *keinen Wert* zurück gibt, wird anstelle eines Datentyps das Schlüsselwort `void` aufgeführt.

### Funktionsname
Der Name der Funktions ist grundsätzlich frei wählbar. Es gelten die gleichen Regeln wie für alle anderen *Bezeichner* (also beispielsweise auch für die Namen von Variablen). Zu den gültigen Zeichen zählen Gross- und Kleinbuchstaben, Ziffern sowie der Untenstrich (`_`). Dabei muss der Name zwingend mit einem Buchstaben beginnen.
Weil *Methoden* gemäss gängiger Konvention *gross* geschrieben werden (*PascalCase*), kann diese Konvention auch auf *Funktionen* angewendet werden.

Weitere Konventionen für die Wahl von Funktionsnamen:

- Verwendung von Verben ist üblich: `CalculateAverage`, `ShowResult`, `SendMessage`, ...
- Präfix `Is` oder `Has` für Methoden mit einem Rückgabewert vom Datentyp `boolean`: `HasBirthday`, `IsBiggerThanAverage`, ...

### Parameterliste
Für jede Funktion wird eine, möglicherweise leere, Liste von Parametern angegeben. Jeder Parameter in dieser Liste wird durch einen Datentyp und einen Namen definiert. Der Wert eines Parameters wird beim Aufruf der Funktion festgelegt und kann innerhalb der Funktion durch Nennung des entsprechenden Parameternamens verwendet werden.
Die Namen der einzelnen Parameter innerhalb der Parameterliste müssen einzigartig sein. Bezüglich der Datentypen gibt es keine Einschränkungen - es dürfen beliebig viele Parameter jedes Datentyps verwendet werden.
Die Reihenfolge der Parameter ist bei der Definition der Funktion frei wählbar - beim Aufruf der Funktion muss jedoch die Reihenfolge gemäss Parameterliste eingehalten werden.

## Aufruf einer Funktion
Eine Funktion wird durch Nennung ihres Funktionsnamens und Angabe von Werten für alle (nicht optionelen) Parameter aufgerufen. Dabei müssen die Reihenfolge der Parameter sowie die Datentypen gemäss Funktionsdefinition beachtet werden.
Ein allfälliger Rückgabewert der Funktion kann für eine Zuweisung oder innerhalb eines Ausdrucks verwendet werden.

## Beispiele
Im nachfolgenden Code-Beispiel werden einige Funktionen definiert und Aufgerufen.

```c#
// Function without return value (void) and three parameters.
void PrintAverage(int a, int b, int c)
{
    float average = (a + b + c) / 3.0f;
    Console.WriteLine($"Sum of {a}, {b} and {c} is: {average}");
}

// Call function PrintAverage
PrintAverage(2, -4, 8); // Sum of 2, -4 and 8 is: 2

// Function which returns a boolean value and takes two parameters.
bool IsAboveAverage(int valueToCheck, int[] numbers)
{
    float average = 0;
	for (int i = 0; i < numbers.Length; i++)
	{
		average += numbers[i] / (float)numbers.Length;
	}
	
	return valueToCheck > average;
}

int myFavoriteNumber = 9;

// The return value of the function is used as the condition.
if (IsAboveAverage(myFavoriteNumber, new int[] {2, 8, -1, 19, 3}))
{
    Console.WriteLine($"Yeah, {myFavoriteNumber} is above average!");
}
```