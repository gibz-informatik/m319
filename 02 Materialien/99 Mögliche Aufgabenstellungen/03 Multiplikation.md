# Multiplikation

Für die Multiplikation wird üblicherweise der entsprechende Multiplikationsoperator `*` verwendet. Aus mathematischer Sicht kann die Multiplikation jedoch auch als *wiederholte Addition* betrachtet werden:

```math
3 * 8 = 8 + 8 + 8 = 24
```

```math
8 * 3 = 3 + 3 + 3 + 3 + 3 + 3 + 3 + 3 = 24
```

Erstellen Sie ein Programm, welches das Produkt aus zwei Zahlen berechnet. Verwenden Sie den Multiplikationsoperator `*` dabei *nicht*.

## Varianten

- Erstellen Sie ein ähnliches Programm für die Berechnung von Potenzen. Berechnen Sie beispielsweise die Quadratzahl von 8 ($`8^2 = 64`$) oder die vierte Potenz von 7 ($`7^4 = 2401`$).
- Implementieren Sie das Programm so, dass dieses auch für negative Zahlen korrekt funktioniert.